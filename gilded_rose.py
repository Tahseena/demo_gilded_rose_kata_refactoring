class GildedRose(object):

    def __init__(self, items):
        self.items = items

    def update_quality(self):
        for item in self.items:
            if item.name == "Aged Brie" or item.name == "Backstage passes to a TAFKAL80ETC concert":
                if item.sell_in < 0:
                    item.quality = 0
                elif item.sell_in < 11:
                    if item.quality < 50:
                        item.quality = item.quality + 2
                elif item.sell_in < 6:
                    if item.quality < 50:
                        item.quality = item.quality + 3
            elif (item.name == "Sulfuras, Hand of Ragnaros" and item.quality==80):
                pass
            elif(item.name == "Conjured Mana Cake" or item.sell_in <0):
                    item.quality = item.quality - 2
            else:
                if item.quality > 0:
                    item.quality -= 1
            if item.name != "Sulfuras, Hand of Ragnaros":
                item.sell_in -= 1

class Item:
    def __init__(self, name, sell_in, quality):
        self.name = name
        self.sell_in = sell_in
        self.quality = quality

    def __repr__(self):
        return f"{self.name}, {self.sell_in}, {self.quality}"
